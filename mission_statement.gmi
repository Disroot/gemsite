```
                        `````                                                                                                                         
                        ``                                                                                                                            
                        ``                                                                                                                            
                        ``                                                                                                                            
                      ````                                                                                                                            
                   ``` ``          .----.    ...  `...  ... .--                                                                                       
                 `     ``        .yNNddNmy. -NNh  -NNy  hNNhmmd                                                                                       
                       ```       dMM+``sMMh -MMd  :MMh  dMMy...                                                                                       
                      ```       `NMM-  /MMd -MMd  +MMh  dMM-                                                                                          
                 ``   ```        +NMmssmMN+ .NMMhhNMMh  dMM.                                                                                          
               ``    ````         -+syys+.   -syyo:oo/  /oo`                                                                                          
              `       ``                                                                                                                              
             `        ``                          /hh.                   /hh.                                                                         
                      ``          `..`.--. .--.   :oo.  .----.`  .----.  -oo.  `.---.`   ..``---`                                                     
                      ``          /MMmmNMNhmNMMy  oMM- sMMdddN/ yMNddmN: oMM- :dMNNNMm+  mMNdNNMm-                                                    
                     ```          /MMy -MMN` dMM  oMM- hMNyso+` dMNyso/` oMM- NMM: `dMM- mMN- oMMo                                                    
                      ``          /MM/ .MMm  hMM  oMM- `/+syMMh ./+syMMy oMM- mMM: .dMM- mMm  +MMo                                                    
                      ``          /MM/ .MMd  hMM  oMM- dNdddMNo mmddmMN+ oMM- -dMMNNMm/  mMd  +MMo                                                    
                  ```  ``          ``   ```  ```   ``  `.----`  `.---.    ``    `---.    ```   ``                                                     
                       ``                                                                                                                             
                       ```                                                                                                                            
                        ``                                                                                                                            
                        ``                                                                                                                            
                       ```                                                                                                                            
                      ` ``                                                                                                                            
                    ` `  ``                                                                                                                            
                        ``                                                                                                                            
                        ``                                                                                                                            
                        ` `                                                                                                                           
                        `  `                                                                                                                           
                       ``   ``                                                                                                                          
                      ```                                                                                                                             
                     `   `                           
```

The mission set by the Disroot.org foundation aims to recover the Internet as the diverse, independent tool it was always meant to be.

The once decentralized, democratic and free internet, has been dominated by a handful of technology giants, promoting concentration in monopolies, more government control and more restrictive regulations. Everything that, in our opinion, opposes and destroys the true essence of this wonderful tool.

By promoting the use of free (libre) and open source software we support collective and community effort towards collaboration. Disroot.org wishes to be an example that there are possible alternatives to the corporate world that is offered in large. Alternatives that are not built on the basis of obtaining economic benefits. Alternatives for which transparency, openness, tolerance and respect are key elements.

# The platform

Disroot.org offers a web services platform built on the principles of privacy, openness, transparency and freedom. Its most important objective from the technical point of view is to provide the best possible service with the best level of support. We chose a working approach in which users (from now on referred to as Disrooters) are the most valuable part and the main beneficiaries of the project; decisions are made with them in mind and not profiting with economic benefit, personal fulfillment, control or power. Disroot.org is committed to defend these key principles in the best possible way.

Online privacy is constantly compromised by those seeking profit and financial control. Our data has become a very valuable commodity and most online companies take advantage of it openly or surreptitiously. Disroot.org is committed to never sell, help process or use any information provided by Disrooters to third parties for any financial, political or power gaining purposes. Disrooters’ data belongs to them and the project simply stores it for those Disrooters to use. Our motto is "The less we know about our users, the better". We implement data encryption whenever possible to ensure that obtaining user data by unauthorized third parties is as difficult as possible and we maintain only the minimum of user logs or data that are essential for the service performance.

# The ambitions

The Disroot.org Foundation has ambitions that go beyond the mere operation of the platform. The intention is to not become a centralized platform but to promote and sustain an ecosystem that helps others establish their own independent nodes. One of the most important long-term goals is to provide space for other organizations, individuals and collectives who want to start their journey so they don't have to reinvent the wheel over and over again. We want to encourage the emergence of more independent, decentralized service providers who work in cooperation - not competition - with each other. Together we can build a truly stronger decentralized network.

We openly promote and encourage the birth of new federated nodes and hope to be able to help other groups set up their Disroot-like platform. In order to support new projects with similar objectives, we are working on a set of tools to facilitate the implementation, maintenance and administration of the services. Another of our ambitions is to create a space for sharing skills and exchanging knowledge, create a legal aid platform to help others deal with rules, laws, regulations and law enforcement, help create new nodes and contribute to sponsoring the development of Libre and Open Source software.
---

# The Practicalities

In order to make the Disroot.org project work according to the above principles, we operate according to the following practical ones:

## Funding

Disroot.org is a non-profit foundation and all revenues from the Disroot platform are invested back into the project, the maintenance of services and into free software development.

We recognize different ways to fund the project and its development.

### Donations from users.

We think that putting a price on our services is, in some way, limiting the autonomy of users. Therefore, we entrust this responsibility to Disrooters. We understand that the value of money varies according to different factors, such as geographical location, wealth status or personal financial situation, so we decided to let the Disrooters themselves evaluate how much they can contribute to the project.

It is important to highlight the fact that nothing on the Internet is free and that online services always have a cost that is paid for with real money or valuable private data. We are sure that Internet freedom can only be achieved and sustained when people realize this and contribute voluntarily. Since Disroot.org does not sell user data, we rely heavily on users donations.

### Grants or subsidies.

The receipt of grants and other forms of funding will only be accepted if they do not harm in any way the independence of the project. We will not jeopardize Disrooters’ privacy, or our freedom and the way we want to manage the service, to meet the requirements set by the “donors”. We are aware that grants are often allocated for a specific purpose or result but we are also convinced that some of those requirements can be pursued without harming the project or the independent federated network as a whole. We will consider every possible grant carefully before accepting it to ensure it doesn’t contradict the main principals of the project.

## Decision-making process
We follow a decision-making process based on consensus. Within the Disroot Core Team everyone has an equal voice and opinion. All important decisions are made during meetings at which proposals are presented and discussed extensively so everyone has time to elaborate their opinions or concerns before reaching mutual agreement.

## Community Inclusion
Disroot.org would not make any sense without the vibrant community that surrounds it, whose active participation in the project is very important and encouraged. We welcome any help, improvements, feedback and suggestions and we offer various ways of communication between members of the Disroot Community and the Core Team, such as: forums, various Taiga project boards or instant messaging rooms.
---

# TL:DR (Too Long, DID Read)

The purpose of Disroot.org is to reclaim the virtual world from the monolithic reality that has taken it over. We are certain that this approach has gigantic potential for uprooting society from current status quo ruled by advertisement, consumerism and the power of the highest bidder. By decentralizing a co-operated network of services in the 21st century, we can overcome the shortcomings imposed by financial limitations, energy footprint and complex infrastructures - giving the network the power to grow independently according to its own rules. A network of many small tribes cooperating with each other will defeat one monolithic giant. Will you come and disroot along with us?
